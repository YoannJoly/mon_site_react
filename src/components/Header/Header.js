import React, { Component, Fragment } from 'react'
// import "./Header.css"
import Logo from '../../images/logo.svg'
import cvpdf from '../../PDF/CV.pdf'

class Header extends Component {

    render() {

        return (
            <Fragment>
                <link rel="preconnect" href="https://fonts.googleapis.com" />
                <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin />
                <link href="https://fonts.googleapis.com/css2?family=Lato:ital,wght@0,100;0,300;0,400;0,700;0,900;1,100;1,300;1,400;1,700;1,900&display=swap" rel="stylesheet" />

                <header class="sm:flex sm:flex-row mt-8 flex flex-col sm:justify-around" >
                    <div class="sm:w-2/3 place-self-center mb-8 sm:mb-0 sm:ml-16" >
                        <a
                            href='/'
                        >
                            <img src={Logo} class="w-44 transition duration-700 ease-in-out transform hover:scale-110" alt="logo" />
                        </a>
                    </div>
                    <nav class="place-self-center flex flex-col sm:flex-row sm:justify-around sm:w-96">
                        <a
                            href={cvpdf}
                            class="text-center bg-white mb-8 sm:mb-0 border border-blue-500 py-3 px-10 rounded-full font-semibold hover:bg-blue-500 text-blue-600 hover:text-white hover:border-blue-700 transition duration-700 ease-in-out transform hover:scale-110"
                            target='_blank'
                            rel='noopener noreferrer'
                            aria-label="CV - Lien s'ouvrant dans un nouvel onglet">CV</a>
                        <a
                            href='/contact'
                            class="text-center bg-white border border-blue-500 py-3 px-10 rounded-full font-semibold hover:bg-blue-500 text-blue-600 hover:text-white hover:border-blue-700 transition duration-700 ease-in-out transform hover:scale-110"
                            aria-label="Page contact - Lien s'ouvrant dans un nouvel onglet">Contact</a>
                    </nav>
                </header>
            </Fragment>
        )
    }
}

export default Header;
