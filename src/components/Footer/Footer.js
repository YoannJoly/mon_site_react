import React, { Component, Fragment } from 'react'
import log from '../../images/logo2.svg'
import linkedin from '../../images/linkedin.svg'
import mail from '../../images/mail.svg'
import copy from '../../images/copyright.svg'

class Footer extends Component {
    render() {
        return (
            <Fragment>
                <footer class="mt-16 pt-4 sm:mt-20 bg-blue-500">
                    <a href='/'><img
                        src={log}
                        alt='Logo'
                        class="w-1/3 mx-1/3 sm:w-1/6 sm:mx-5/12 lg:w-1/9 lg:mx-4/9"
                    />
                    </a>
                    <div class="flex justify-center mt-4">
                        <a href='https://www.linkedin.com/in/yoann-joly-a63288b7/' target='_blank' rel='noopener noreferrer' aria-label="LinkedIn - Lien s'ouvrant dans un nouvel onglet" ><img
                            src={linkedin}
                            alt="logo linkedin"
                            title='linkedin'
                            class="w-14 mr-4"
                        />
                        </a>
                        <a href='mailto:yoann.joly12@gmail.com' target='_blank' rel='noopener noreferrer' aria-label="Mail - Lien s'ouvrant dans un nouvel onglet"><img
                            src={mail}
                            alt='logo mail'
                            title='contact'
                            class="w-14 ml-4"
                        />
                        </a>
                    </div>
                    <div class="flex mt-8 pb-2 flex-col text-center items-center sm:flex-row sm:text-left sm:justify-around lg:justify-start">
                        <div class="flex items-center">
                            <img
                                src={copy}
                                alt='copyright'
                                class="h-4 lg:ml-16"
                            />
                            <a
                                href='https://www.termsfeed.com/live/4d14ee05-ce05-49cd-b358-50353b655c89'
                                target='_blank'
                                title="Voir les mentions légales"
                                rel='noopener noreferrer'
                                aria-label="Mentions légales - Lien s'ouvrant dans un nouvel onglet"
                                class="text-white ml-1"
                            >
                                Mentions légales
                            </a>
                        </div>
                        <a
                            target="_blank"
                            title="Aller sur flaticon"
                            rel="noreferrer"
                            href="https://www.flaticon.com/"
                            class="text-white my-8 sm:my-0 lg:mx-8"
                        >
                            Icons by Freepik
                        </a>
                        <a
                            href="https://www.freepik.com/vectors/background"
                            target='_blank'
                            class="text-white" rel="noreferrer"
                        >
                            Background vector created by freepik - www.freepik.com</a>
                    </div>
                </footer>
            </Fragment>)
    }
}

export default Footer;