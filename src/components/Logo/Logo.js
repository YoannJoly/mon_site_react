import React from 'react';
import Logo from '../../images/logo.svg'

const logo = (props) => (
    <div class="w-1/10">
        <a
            href='/'
        >
            <img src={Logo}  alt="logo" className="logo" />
        </a>
    </div>
);

export default logo;