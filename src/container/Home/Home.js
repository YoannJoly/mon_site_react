import React, { Component } from 'react'
import Header from '../../components/Header/Header'
import Footer from '../../components/Footer/Footer'
import PC from '../../images/pc.svg'
import cl from '../../images/culture live.jpg'
import imgcode from '../../images/code.svg'
import imgwrench from '../../images/wrench.svg'
import imgplus from '../../images/plus.svg'
import certification from '../../PDF/certification.pdf'
// import soulage from '../../images/soulage.jpg'


class Home extends Component {
    render() {
        return (
            <div>
                <Header />
                <main class="mt-10 font-lato">
                    <h1 class="font-black text-center text-4xl text-indigo-700">Développeur Web full-stack</h1>
                    <div class="flex flex-col">
                        <img
                            src={PC}
                            alt='PC'
                            class="mt-8 sm:mt-16 w-2/6 sm:w-1/6 place-self-center"
                        />
                        <div>
                            <div class="flex justify-center bg-blue-500 sm:pb-24 sm:absolute sm:w-full">
                                <div class="text-white items-center text-left w-max pt-8 px-8 pb-8">
                                    <span class="font-semibold">
                                        Bonjour
                                    </span>
                                    <p>
                                        Développeur junior enthousiaste et motivé, j'ai une appétence particulière pour le front et les nouvelles technologies.
                                    </p>
                                    <p>
                                        Accordant de l'importance à l'accessibilité et aux bonnes pratiques du web, je possède une certification <a href="https://www.opquast.com/" target='_blank' class="underline" rel='noopener noreferrer' title="Aller sur le site d'Opquast" aria-label="Site Opquast - Lien s'ouvrant dans un nouvel onglet">Opquast</a> de niveau <a href={certification} target='_blank' class="underline" title="Voir le certificat" rel='noopener noreferrer' aria-label="Certification PDF - Lien s'ouvrant dans un nouvel onglet">avancé.</a>
                                    </p>
                                    <p>
                                        Autonome, curieux et rigoureux, j'aime tout autant travailler sur des projets simples que sur des projets innovants et complexes.
                                    </p>
                                    <p>
                                        Effectuant une veille technologique constante, je n'ai pas peur de l'inconnu et ai soif de découvrir ce qui fera le web de demain.
                                    </p>
                                </div>
                            </div>
                            <div class="flex flex-col items-center text-center pt-8 sm:flex-row sm:w-2/3 sm:mx-1/6 sm:items-start sm:relative sm:bg-white sm:mt-64 md:mt-48 sm:rounded-3xl sm:shadow-2xl sm:border-t sm:border-black md:top-16 lg:top-0">
                                <div class=" flex flex-col border-b border-black w-2/4 pb-4 sm:w-1/3 sm:border-b-0 ">
                                    <span class="font-bold text-xl text-pink-900">Langages</span>
                                    <img
                                        src={imgcode}
                                        alt='Code'
                                        class="w-8 self-center my-4"
                                    />
                                    <ul>
                                        <li>HTML</li>
                                        <li>CSS</li>
                                        <li>Ladder</li>
                                        <li>Javascript</li>
                                        <li>PHP</li>
                                        <li>SQL</li>
                                        <li>Java</li>
                                    </ul>
                                </div>
                                <div class=" flex flex-col mt-4 border-b border-black w-2/4 pb-4 sm:w-1/3 sm:border-b-0 sm:mt-0 sm:border-l sm:border-r">
                                    <span class="font-bold text-xl text-pink-900">Frameworks</span>
                                    <img
                                        src={imgwrench}
                                        alt='Wrench'
                                        class="w-8 self-center my-4"
                                    />
                                    <ul>
                                        <li>React-native</li>
                                        <li>React</li>
                                        <li>Laravel</li>
                                        <li>Wordpress</li>
                                        <li>Angular</li>
                                        <li>Tailwind CSS</li>
                                    </ul>
                                </div>
                                <div class=" flex flex-col mt-4 sm:w-1/3 sm:mt-0">
                                    <span class="font-bold text-xl text-pink-900">Autre</span>
                                    <img
                                        src={imgplus}
                                        alt='Plus'
                                        class="w-8 self-center my-4"
                                    />
                                    <ul>
                                        <li>Figma</li>
                                        <li>Git</li>
                                        <li>Bash</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <h2 class="font-bold text-3xl text-indigo-700 text-center mt-24 mb-14">Projet d'étude</h2>
                    <div >
                        <a
                            href='/culturelive' title="Projet Culture Live">
                            <img
                                src={cl}
                                alt="culture live"
                                class="w-2/3 mx-1/6 lg:w-1/3 lg:mx-1/3 rounded-2xl transition duration-700 ease-in-out transform hover:scale-110 shadow-2xl"
                            />
                        </a>
                        {/* <img src={soulage} alt="siecle soulage" /> */}
                    </div>
                </main>
                <Footer />
            </div>
        )
    }
}

export default Home;
