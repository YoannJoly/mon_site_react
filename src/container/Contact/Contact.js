import React, { Component } from 'react'
import Header from '../../components/Header/Header'
import Footer from '../../components/Footer/Footer'

class Contact extends Component{
    constructor(props) {
        super(props);
        this.state = { feedback: '', name: '', email: '' };
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    render() {
        return (
            <div>
                <Header />
                <form onSubmit={this.handleSubmit}>
                    <h1 class="my-12 text-center font-bold text-2xl text-pink-900">Contactez-moi</h1>
                    <div class="flex flex-col w-4/5 mx-1/10 sm:w-1/2 sm:mx-1/4">
                        <label
                            for="input-name"
                            class="mb-1 font-medium"
                        >Nom:
                        </label>
                        <input
                            type="text"
                            value={this.state.name}
                            onChange={event => this.setState({ name: event.target.value })}
                            id="input-name"
                            class="border border-indigo-300 px-2 shadow-inner lg:px-4 lg:py-4"
                            required
                            aria-required="true"
                        />
                        <label
                            for="input-email"
                            class="mt-4 mb-1 font-medium"
                        >Email:
                        </label>
                        <input
                            value={this.state.email}
                            id="input-email"
                            required
                            aria-required="true"
                            onChange={event => this.setState({ email: event.target.value })}
                            class="border border-indigo-300 shadow-inner px-2 lg:px-4 lg:py-4"
                            type="text"
                        />
                        <label
                            for="text-message"
                            class="mt-4 mb-1 font-medium"
                        >Message:
                        </label>
                        <textarea
                            onChange={this.handleChange}
                            id="text-message"
                            required
                            aria-required="true"
                            value={this.state.feedback}
                            class="border border-indigo-300 h-32 shadow-inner px-2 lg:px-4 lg:py-4 lg:h-36"
                        />
                        <input
                            type="submit"
                            value="Envoyer"
                            class="mt-8 w-2/4 bg-white place-self-center bg-transparent border-2 border-indigo-300 rounded-xl font-semibold px-8 py-1 hover:bg-blue-500 text-blue-600 hover:text-white hover:border-blue-700 transition duration-700 ease-in-out transform hover:scale-110 shadow-lg cursor-pointer lg:w-1/4 lg:py-2"
                        />
                    </div>
                </form>
                <Footer />
            </div>
        )
    }

    handleChange(event) {
        this.setState({ feedback: event.target.value })
    }

    handleSubmit(event) {
        const templateId = 'template_8kpvItlw';
        event.preventDefault();

        if (/^\w+([- || . ]?\w+)*@\w+([-]?\w+)*(\.\w{2,3}\s*)+$/.test(this.state.email)) {
            this.sendFeedback(templateId, { message_html: this.state.feedback, from_name: this.state.name, reply_to: this.state.email });
        } else {
            alert("Adresse email invalide")
        }
    }

    sendFeedback(templateId, variables) {
        window.emailjs.send(
            'gmail', templateId,
            variables
        ).then(res => {
            window.location = '/thanks';
            console.log('Email successfully sent!')
        })
            // Handle errors here however you like, or use a React error boundary
            .catch(err => console.error('Oh well, you failed. Here some thoughts on the error that occured:', err))

    }
}

export default Contact;