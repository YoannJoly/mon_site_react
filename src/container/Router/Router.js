import React, { Component } from 'react'
import { Route, Switch, Redirect } from 'react-router-dom'


import Home from '../Home/Home'
import Contact from '../Contact/Contact'
import Thanks from '../Thanks/Thanks'
import CultureLive from '../CultureLive/CultureLive'

class Router extends Component {
    render() {
        return (
            <Switch>
                <Route path="/" exact component={Home} />
                <Route path='/contact' exact component={Contact} />
                <Route path='/thanks' exact component={Thanks} />
                <Route path='/culturelive' exact component={CultureLive}/>
                <Redirect to="/" />
            </Switch>
        );
    }
}

export default Router; 