import React, { Component } from 'react'
import Mail2 from '../../images/mail2.svg'

class Confirm extends Component {

    render() {
        return (
            <div class="flex flex-col items-center">
                <img 
                src={Mail2} 
                alt='mail' 
                class="w-2/4 mt-16 animate-bounce sm:w-1/4 lg:w-1/5" 
                />
                <span class="font-bold text-2xl mt-4">Message envoyé</span>
                <a 
                href="/"
                class="mt-8 underline"
                >Retour à l'acceuil
                </a>
            </div>
        )
    }
}

export default Confirm;