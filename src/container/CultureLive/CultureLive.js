import React, { Component } from 'react'
import Header from '../../components/Header/Header'
import Footer from '../../components/Footer/Footer'
import cl from '../../images/culture live.jpg'
import ddp from '../../PDF/Dossier de Projet.pdf'
import ipdf from '../../images/ipdf.png'

class CultureLive extends Component {
    render() {
        return (
            <div>
                <Header />
                <main>
                    <div>
                        <h1><img 
                        src={cl} 
                        alt="culture live"
                        class="w-2/3 mx-1/6 my-12 sm:w-1/2 sm:mx-1/4 "
                        />
                        </h1>
                        <h2 class="mb-8 text-center font-bold text-xl text-pink-900">Présentation de l'entreprise</h2>
                        <p class="text-left mx-4 sm:mx-16 lg:mx-48 lg:text-lg">
                            Culture Live est une start-up ayant pour objectif de répertorier tous les événements de la région Occitanie et de proposer une billetterie pour chacun d’entre eux, elle inclut également une description détaillée pour chaque événement ainsi que la possibilité de les noter.<br />
                            Pour accomplir cela elle dispose d’un site web, d’un iframe sous forme de calendrier que les mairies peuvent afficher sur leur site et d’une application mobile.
                        </p>
                        <h3 class="my-4 text-center font-bold text-xl text-pink-900">Ma mission</h3>
                        <p class="text-left mx-4 sm:mx-16 lg:mx-48 lg:text-lg">
                            Cette mission a été réalisée dans le cadre d’un stage de 10 semaines durant ma formation “développeur web / web mobile.<br />
                            Elle constituait à créer un nouveau menu sur l’application développée en React native, il s’agissait du menu “Guichet” permettant l’achat de billets lié aux événements proposés.
                        </p>
                        <br />
                        <p class="text-left mx-4 sm:mx-16 lg:mx-48 lg:text-lg">
                            Pour plus de détails, je vous invite à voir mon dossier de projet ci-dessous.
                        </p>
                        <a 
                        href={ddp} 
                        target='_blank' 
                        rel='noopener noreferrer' 
                        aria-label="Dossier de Projet - Lien s'ouvrant dans un nouvel onglet" 
                        class="flex mt-12 items-center justify-center underline transition duration-700 ease-in-out transform hover:scale-110 text-lg">
                            <img 
                            src={ipdf} 
                            alt="pdf logo"
                            class="h-8"
                            />Dossier de Projet</a>
                    </div>
                </main>
                <Footer />
            </div>

        )
    }
}

export default CultureLive;