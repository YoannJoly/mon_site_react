module.exports = {
  purge: ['./src/**/*.{js,jsx,ts,tsx}', './public/index.html'],
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
      fontFamily: {
        'lato': ['Lato', 'Arial', 'sans-serif']
      },
      margin: {
        '1/6': '16.67%',
        '1/3': '33.33%',
        '5/12': '41.666667%',
        '4/9': '44.444445%',
        '1/4': '25%',
        '1/10': '10%'
      },
      width: {
        '1/9': '11.111112%'
      },
    },
  },
  variants: {
    extend: {},
  },
  plugins: [],
}
